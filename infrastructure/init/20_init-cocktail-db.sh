#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
\c cocktails
  DROP TABLE IF EXISTS cocktails CASCADE;
CREATE TABLE cocktails
(
    name char(36) NOT NULL,
    description char(256) NOT NULL,
    alcohol    boolean           DEFAULT false,
    CONSTRAINT UC_cocktails_name UNIQUE (name)
);
ALTER TABLE cocktails OWNER to "cocktail";
EOSQL
