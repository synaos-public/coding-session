#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER cocktail WITH PASSWORD 'cocktail';
    CREATE DATABASE cocktails;
    GRANT ALL PRIVILEGES ON DATABASE cocktails TO cocktail;

EOSQL
