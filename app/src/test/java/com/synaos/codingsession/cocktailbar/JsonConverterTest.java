package com.synaos.codingsession.cocktailbar;

import com.synaos.codingsession.cocktailbar.model.Cocktail;
import com.synaos.codingsession.cocktailbar.utils.JsonConverter;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;

public class JsonConverterTest {

    @Test
    public void checkJsonConverter() throws Exception {
        Cocktail c = new Cocktail("name", "description", true);
        String json = JsonConverter.convertfromcocktailtoJSON(c);

        System.out.println(json);
        assertThat(json).isNotEmpty();
        assertThat(JsonConverter.isCocktailWithAlcohol(c)).isTrue();
    }
}