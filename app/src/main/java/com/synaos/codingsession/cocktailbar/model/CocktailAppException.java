package com.synaos.codingsession.cocktailbar.model;

public class CocktailAppException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public CocktailAppException(String message) {
        super(message);
    }
}