package com.synaos.codingsession.cocktailbar;

import com.synaos.codingsession.cocktailbar.model.*;
import java.util.Collection;

public interface Printer {
    String print(Collection<Cocktail> cocktails);
}