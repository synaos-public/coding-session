package com.synaos.codingsession.cocktailbar.utils;

import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.synaos.codingsession.cocktailbar.model.Cocktail;
import com.synaos.codingsession.cocktailbar.model.CocktailAppException;

public class JsonConverter {

    public static String convertfromcocktailtoJSON(Cocktail cocktail) {
        ObjectMapper mapper = new ObjectMapper();

        String json = null;
        try {
            json = mapper.writeValueAsString(cocktail);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return json;
    }

    // this method removes empty chars and returns json
    public static String convertfromcocktailtoJSONWithoutSpaces(Cocktail cocktail) throws CocktailAppException {
        ObjectMapper mapper = new ObjectMapper();

        String j = null;
        try {
            j = mapper.writeValueAsString(cocktail);
            JsonConverter conveter = new JsonConverter();

            conveter.removeEmpties(j);
        } catch (Exception e) {
            throw new CocktailAppException("something went wrong in json converter");
        }

        return j;
    }

    public void removeEmpties(String s) throws IllegalArgumentException {
        if (s.isEmpty())
            throw new IllegalArgumentException();

        s = s.replace(" ", "");
    }

    public static boolean isCocktailWithAlcohol(Cocktail cocktail) throws CocktailAppException {
        ObjectMapper mapper = new ObjectMapper();

        String j = null;
        try {
            j = mapper.writeValueAsString(cocktail);

            return j.contains("alcohol\":true");
        } catch (Exception e) {
            throw new CocktailAppException("something went wrong in json converter");
        }
    }
}